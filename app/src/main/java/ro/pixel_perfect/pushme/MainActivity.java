package ro.pixel_perfect.pushme;

import android.content.Context;
import android.graphics.Typeface;
import android.media.MediaPlayer;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity {

    private Context mContext;
    private ImageView mTheButton;
    private TextView mTheScore;
    private static long score = 0;
    private static MediaPlayer mediaPlayer;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        init();
    }

    private void init() {
        mContext = getApplicationContext();
        mTheButton = (ImageView) findViewById(R.id.theButton);
        mTheScore = (TextView) findViewById(R.id.theScore);
        Typeface font = Typeface.createFromAsset(mContext.getAssets(), "fonts/kirbyss.ttf");
        mTheScore.setTypeface(font);
        mTheButton.setOnClickListener(buttonClickListener);
        mediaPlayer = MediaPlayer.create(this, R.raw.sound);
    }

    View.OnClickListener buttonClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            mTheButton.setImageResource(R.drawable.button_pushed);
            if (mediaPlayer.isPlaying()) {
                mediaPlayer.stop();
                mediaPlayer.release();
                mediaPlayer = MediaPlayer.create(mContext, R.raw.sound);
            }
            mediaPlayer.start();
            score++;
            mTheScore.setText(String.valueOf(score));
            mTheButton.setImageResource(R.drawable.button);
        }
    };
}
